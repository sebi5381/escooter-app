//
//  eScooter_appApp.swift
//  eScooter-app
//
//  Created by Sebastian Pomirleanu on 12.04.2021.
//

import SwiftUI

@main
struct eScooter_appApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
