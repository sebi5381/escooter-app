//
//  OnboardingScreen.swift
//  eScooter-app
//
//  Created by Sebastian-Mihai Pomirleanu on 05.05.2021.
//

import Foundation
import SwiftUI



struct OnboardingScreen: View {
    
    var viewModel: OnboardingScreenViewModel = OnboardingScreenViewModel()
    
    var body: some View {
        content
    }
    
    var content: some View {
        VStack {
            mainImage
            VStack(alignment: .leading, spacing: 0) {
                titleAndSkip
                    .padding(.top, 24)
                textContent
                    .padding(.top, 12)
                    .padding(.trailing, (viewModel.currentScreenIndex == 0 || viewModel.currentScreenIndex == 1) ? (68-24) : 0)
                Spacer()
                navigationDotsAndButton
                    .padding(.bottom, 74)
            }
            .padding([.leading, .trailing], 24)
        }
        .ignoresSafeArea()
    }
    
    
    var titleAndSkip: some View {
        HStack{
            title
            Spacer()
            skip
        }
    }
    
    var navigationDotsAndButton: some View {
        HStack{
            navigationDots
            Spacer()
            navigationButton
        }
    }
    
    
    
    var mainImage: some View {
        viewModel.images[viewModel.currentScreenIndex]
            .resizable()
            .ignoresSafeArea()
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 2, alignment: .top)
    }
    
    var title: some View {
        Text(viewModel.titles[viewModel.currentScreenIndex])
            .font(.customHeading1)
            
    }
    
    var skip: some View {
        Text("Skip")
            .font(.customButton2)
    }
    
    var textContent: some View {
        Text(viewModel.texts[viewModel.currentScreenIndex])
            .font(.customBody1)
            .foregroundColor(.customDarkBlue)
            .frame(alignment: .leading)
            .lineSpacing(24 - 16)
    }
    
    var navigationDots: some View {
        ForEach(0 ..< viewModel.totalScreens) { index in
            RoundedRectangle(cornerRadius: 1.5)
                .fill(index == viewModel.currentScreenIndex ? Color.customDarkPurple : Color.customPurpleishGray)
                .frame(width: index == viewModel.currentScreenIndex ? 16 : 4, height: 4)
        }
    }
    
    var navigationButton: some View {
        Button(action: {
            
        }, label: {
            navigationButtonContent
        })
    }
    
    var navigationButtonContent: some View {
        RoundedRectangle(cornerRadius: 16)
            .fill(Color.customScreamingRed)
            .frame(width: viewModel.isLastScreen ? CGFloat(153) : CGFloat(100), height: 56, alignment: .center)
            .overlay(
                HStack{
                    Text(viewModel.isLastScreen ? "Get Started" : "Next")
                        .font(.customBody1)
                        .foregroundColor(.customWhite)
                    Image.symbolArrowRight
                }
            )
    }
}











class OnboardingScreenViewModel: ObservableObject {
    var images: [Image] = []
    var texts: [String] = []
    var titles: [String] = []
    @Published var currentScreenIndex: Int = 0
    
    var isLastScreen: Bool {
        if currentScreenIndex == totalScreens - 1 {
            return true
        }
        return false
    }
    
    var totalScreens: Int {
        return images.count
    }
    
    init() {
        addScreen(mainImage: Image.onboarding1, title: "Safety", textContent: "Please wear a helmet and protect yourself while riding")
        addScreen(mainImage: Image.onboarding2, title: "Scan", textContent: "Scan the QR code or NFC sticker on top of the scooter to unlock and ride.")
        addScreen(mainImage: Image.onboarding3, title: "Ride", textContent: "Step on the scooter with one foot and kick off the ground. When the scooter starts to coast, push the right throttle to accelerate.")
        addScreen(mainImage: Image.onboarding4, title: "Parking", textContent: "If convenient, park at a bike rack. If not, park close to the edge of the sidewalk closest to the street. Do not block sidewalks, doors or ramps.")
        addScreen(mainImage: Image.onboarding5, title: "Rules", textContent: "You must be 18 years or and older with a valid driving licence to operate a scooter. Please follow all street signs, signals and markings, and obey local traffic laws.")
    }
    
    func addScreen(mainImage: Image, title: String, textContent: String) {
        images.append(mainImage)
        titles.append(title)
        texts.append(textContent)
    }
    
    func next() {
        currentScreenIndex += 1
        if currentScreenIndex >= titles.count {
            currentScreenIndex = 0
        }
    }
}







struct OnboardinScreen_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingScreen()
    }
}
