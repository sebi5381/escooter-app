//
//  SplashScreen.swift
//  eScooter-app
//
//  Created by Sebastian-Mihai Pomirleanu on 04.05.2021.
//

//import Foundation
import SwiftUI

struct SplashScreen: View {
    
    var body: some View {
        ZStack {
            Color.customDarkPurple
            Image.moveRectangle
            Image.locationPoint
            HStack() {
                Image.halfScooter
                Spacer()
            }
            Image.moveText
        }
        .ignoresSafeArea()
        
    }
}



struct SplashScreen_Previews: PreviewProvider {
    static var previews: some View {
        SplashScreen()
    }
}
