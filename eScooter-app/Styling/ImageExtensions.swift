//
//  ImageExtensions.swift
//  eScooter-app
//
//  Created by Sebastian-Mihai Pomirleanu on 04.05.2021.
//

import SwiftUI


extension Image {
    public static let moveRectangle = Image("moveRectangle")
    public static let locationPoint = Image("locationPoint")
    public static let halfScooter = Image("halfScooter")
    public static let moveText = Image("moveText")
    
    public static let onboarding1 = Image("onboardingSafety")
    public static let onboarding2 = Image("onboardingScan")
    public static let onboarding3 = Image("onboardingRide")
    public static let onboarding4 = Image("onboardingParking")
    public static let onboarding5 = Image("onboardingRules")
    
    public static let symbolArrowRight = Image("arrow-right")
}
