//
//  FontExtensions.swift
//  eScooter-app
//
//  Created by Sebastian-Mihai Pomirleanu on 05.05.2021.
//

import SwiftUI



extension Font {
    static let customHeading1 = Font.custom(FontNames.baiJamjureeBold, size: 32)
    static let customHeading2 = Font.custom(FontNames.baiJamjureeMedium, size: 20)
    static let customHeading3 = Font.custom(FontNames.baiJamjureeSemiBold, size: 17)
    static let customHeading4 = Font.custom(FontNames.baiJamjureeMedium, size: 16)
    
    
    static let customButton1 = Font.custom(FontNames.baiJamjureeBold, size: 16)
    static let customButton2 = Font.custom(FontNames.baiJamjureeMedium, size: 14)
    
    static let customCaption1 = Font.custom(FontNames.baiJamjureeSemiBold, size: 12)
    static let customCaption2 = Font.custom(FontNames.baiJamjureeRegular, size: 12)
    
    static let customBody1 = Font.custom(FontNames.baiJamjureeMedium, size: 16)
    static let customBody2 = Font.custom(FontNames.baiJamjureeMedium, size: 14)
    
    static let customSmallText = Font.custom(FontNames.baiJamjureeRegular, size: 12)
}

class FontNames {
    static let baiJamjureeBold = "BaiJamjuree-Bold"
    static let baiJamjureeMedium = "BaiJamjuree-Medium"
    static let baiJamjureeSemiBold = "BaiJamjuree-SemiBold"
    static let baiJamjureeRegular = "BaiJamjuree-Regular"
}
