//
//  ColorExtensions.swift
//  eScooter-app
//
//  Created by Sebastian-Mihai Pomirleanu on 04.05.2021.
//

import SwiftUI


extension Color {
    public static let customDarkBlue = Color("customDarkBlue")
    public static let customDarkPurple = Color("customDarkPurple")
    public static let customGreenishGray = Color("customGreenishGray")
    public static let customPink = Color("customPink")
    public static let customPurpleishGray = Color("customPurpleishGray")
    public static let customScreamingRed = Color("customScreamingRed")
    public static let customWhite = Color("customWhite")
    public static let customWhiteishGray = Color("customWhiteishGray")
}
